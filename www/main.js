/*
 * おまじない
 */
enchant();


/*
 * 定数
 */
// パラメータ
var SCREEN_WIDTH    = 320;  // スクリーンの幅
var SCREEN_HEIGHT   = 320;  // スクリーンの高さ
// プレイヤー
var PLAYER_WIDTH    = 16;   // 幅
var PLAYER_HEIGHT   = 25;   // 高さ
var PLAYER_JUMP     = -15;   // ジャンプの強さ
var PLAYER_GRAVIRY  = 5; // 重力
// 敵
var ENEMY_WIDTH     = 32;   // 幅
var ENEMY_HEIGHT    = 32;   // 高さ
var ENEMY_SPEED     =-5;    // 移動スピード
var ENEMY_HIT_LENGTH= 20;   // 衝突判定の領域サイズ
// アイテム
var ITEM_WIDTH      = 16;   // 幅
var ITEM_HEIGHT     = 16;   // 高さ
var ITEM_SPEED      =-4;    // アイテムのスピード
var COIN_POINT      = 100;  // コインのポイント
var COIN_FRAME      = 14;   // コイン画像のフレームインデックス
var DIAMOND_POINT   = 1000; // ダイアモンドのポイント
var DIAMOND_FRAME   = 64;   // ダイアモンドのフレームインデックス
// 背景
var BACKGROUND_WIDTH    = 1320;
var BACKGROUND_HEIGHT   = 320;
// 画像
var PLAYER_IMAGE1        = "images/ninja/ninja3.png";
var PLAYER_IMAGE2        = "images/ninja/ninja1.png";
var PLAYER_IMAGE3        = "images/ninja/ninja3.png";
var PLAYER_IMAGE4        = "images/ninja/ninja2.png";
var ENEMY_IMAGE         = "images/chara1.png";
var ICON_IMAGE          = "images/icon0.gif";
var BACKGROUND_IMAGE    = "images/background.png";
var GROUND_IMAGE3        = "images/ground1-2.png";
var G_WIDTH = 32;
var G_HEIGHT = 32;
var BTN_IMAGE        = "images/btn.png";
var BTN_JUMP        = "images/btn_jump.png";
var BTN_SHURIKEN    = "images/btn_shuriken.png";
var ATTACK_WIDTH = 16;
var ATTACK_HEIGHT = 16;
var HINOTAMA_IMAGE1 = "images/ninja/hinotama1.png";
var HINOTAMA_IMAGE2 = "images/ninja/hinotama2.png";
var ATTACK_IMAGE = "images/ninja/shuriken.png";

// 画像変更の必要があるかもしれない。
var ATTACK_EFFECT    = "images/effect0.png";
var OROCHI_IMAGE     = "images/monster5.gif";
var CLEAR_IMAGE      = "images/clear.png";

//足軽
var ASHIGARU_IMAGE1         = "images/ninja/musha3.png";
var ASHIGARU_IMAGE2         = "images/ninja/musha1.png";
var ASHIGARU_IMAGE3         = "images/ninja/musha3.png";
var ASHIGARU_IMAGE4         = "images/ninja/musha2.png";
var ASHIGARU_WIDTH = 25;
var ASHIGARU_HEIGHT = 33;

// アセット
var ASSETS = [
    PLAYER_IMAGE1, PLAYER_IMAGE2, PLAYER_IMAGE3, PLAYER_IMAGE4, ENEMY_IMAGE, ICON_IMAGE, BACKGROUND_IMAGE,GROUND_IMAGE3,
    BTN_IMAGE,BTN_JUMP,BTN_SHURIKEN,ATTACK_EFFECT,OROCHI_IMAGE,CLEAR_IMAGE,ASHIGARU_IMAGE1, ASHIGARU_IMAGE2, ASHIGARU_IMAGE3, ASHIGARU_IMAGE4,HINOTAMA_IMAGE1,HINOTAMA_IMAGE2,ATTACK_IMAGE
];


/*
 * グローバル変数
 */
var game        = null;
var player      = null;
var scoreLabel  = null;

/*
 *  地面と落とし穴
 */
var is_ground = [
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体１分くらい */-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体１分くらい */-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体１分くらい */-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体１分くらい */-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体１分くらい */-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 1,-1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
//            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
//            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
//    
//            [-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
//            [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,-1, 1,-1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,-1,-1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,-1, 1,-1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//            [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 0, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1, 1, 1,-1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],

// 仮のステージ開始
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 0,-1,-1,-1,-1,-1, 0, 0,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体１分くらい */-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 0,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1, 0,-1, 0,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 0, 1, 1,-1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1],
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1, 0,-1, 0, 0,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1, 0, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体１分くらい */-1,-1, 0, 0,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1, 0, 0, 1,-1, 1,-1, 1,-1, 1,-1, 0, 0, 0, 0,-1,-1,-1,-1, 0, 1,-1,-1, 0, 0,-1,-1,-1,-1,-1, 0,-1,-1,-1, 0,-1, 0,-1, 0, 0, 1, 1,-1, 0,-1,-1,-1, 0, 0,-1,-1,-1,-1,-1,-1, 0,-1,-1, 0,-1, 0, 0, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 1,-1, 1, 1, 1,-1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1],
            [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1, 0, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1,-1, 0, 1,-1, 0,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1, 0,-1,-1, 0,-1, 0,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1, 0,-1, 0,/* 大体１分くらい */-1, 0, 1, 1,-1,-1,-1, 0, 0, 1, 0,-1, 0, 0, 1, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1, 1, 1, 1, 0,-1, 0, 0, 1, 1,-1,-1, 1, 1, 0, 0,-1, 0, 0, 1, 0,-1, 0, 1,-1, 1,-1, 1, 1, 1, 1, 0, 1,-1, 0, 0, 1, 1,-1,-1,-1,-1, 0, 0, 1, 0, 0, 1,-1, 1, 1, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1,-1, 0,-1, 0,-1, 0,-1, 1, 1,-1, 1, 0,-1,-1,-1,-1, 0, 0,-1, 0, 1, 1,-1, 1, 1, 1,-1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,/* 大体２分くらい */ -1,/* ボス戦 */ -1,-1,-1,-1,-1,-1,-1, 0, 1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1],
            
            [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0,-1, 0, 0, 0, 0, 1,-1,-1, 0, 0,-1, 0, 0, 0, 1, 1,-1, 0, 1, 1,-1, 1,-1, 0, 0, 0, 0, 0, 0, 1, 0,-1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0,-1,-1, 0, 0, 1, 0,-1, 0, 0, 1, 0, 0, 1, 0, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1, 0, 0, 1,-1, 1,/* 大体１分くらい */-1, 1, 1, 1, 0,-1, 0, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1,-1,-1, 1, 1, 1, 1,-1, 1, 1, 1, 1,-1, 1, 1,-1, 1,-1, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 0,-1, 0, 0, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1,-1, 1,-1, 1,-1, 1,-1, 1, 1,-1, 1, 1, 0,-1, 0, 0, 1, 1,-1, 1, 1, 1,-1, 1, 1, 1,-1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,/* 大体２分くらい */  0,/* ボス戦 */  0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
            
            [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1,-1,-1, 1, 1,-1, 1, 1, 1, 1, 1,-1, 1, 1, 1,-1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1,-1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1, 1, 1, 1,-1, 1,/* 大体１分くらい */-1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1,-1,-1, 1, 1, 1, 1,-1, 1, 1, 1, 1,-1, 1, 1,-1, 1,-1, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1,-1, 1,-1, 1,-1, 1,-1, 1, 1,-1, 1, 1, 1,-1, 1, 1, 1, 1,-1, 1, 1, 1,-1, 1, 1, 1,-1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,/* 大体２分くらい */  1,/* ボス戦 */  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ];

/*
 * 汎用処理
 */
// ランダム値生成
var randfloat = function(min, max) {
    return Math.random()*(max-min)+min;
};
var stage = new Group();


/*
 * メイン処理
 */
window.onload = function() {
    // ゲームオブジェクトの生成
    game = new Game(SCREEN_WIDTH, SCREEN_HEIGHT);
    // 画像, SEの読み込み
    game.preload(ASSETS);
    
    // 敵インスタンスの配列化
    enemies = [];
    
    // ゲーム開始時の処理
    game.onload = function() {
        var scene = game.rootScene;
        scene.backgroundColor = "#0af";
        
        scene.onenter = function() {
            // ゲームのフレームを初期化
            game.frame = 0;
            game.pushScene(game.makeStage1());
        };
    	
    };

    // ステージ１開始時の処理
    game.makeStage1 = function() {
        var scene = new Scene();
        var scollCount = 0;
        var orochi = null;
    	// 横スクロール背景を生成, 表示
        var background = new Sprite(BACKGROUND_WIDTH, BACKGROUND_HEIGHT);
        background.image = game.assets[BACKGROUND_IMAGE];
        background.moveTo(0, 0);
        background.onenterframe = function() {
            // スクロール
            this.x -= 1;//変更⚫️
            // 端まで行ったら戻す
            if (this.x <= -(BACKGROUND_WIDTH-SCREEN_WIDTH)) {
                background.moveTo(0, 0);
                console.log("スクロールログカウント: " + scollCount);
                scollCount++;
                if (scollCount > 3 && orochi === null ) {
                	
                    orochi = new Orochi();
                        
                    scene.addChild(orochi);
                    // フレームでのキー管理
                    orochi.key = game.frame;
                    enemies[orochi.key] = orochi;
                }
            }
        };
        scene.addChild(background);
        
        //ステージ
        scene.addChild(stage);
        
        // シーン切替時の処理
        scene.onenter = function() {
            
            //地面
	        map = new Map(32, 32);
	        map.image = game.assets[GROUND_IMAGE3];
	        map.loadData(is_ground);
	        stage.addChild(map);
	        map.onenterframe = function(){
	            // 移動
	            //this.x -= 1;
	            
	            // フレームアニメーション
	            //this.frame = 1;
	        }
            
            // ジャンプボタン用(左下配置)
            var aBtn = new Button(20, SCREEN_HEIGHT - 55, 'a');
    		scene.addChild(aBtn);
            aBtn.ontouchstart = function() {
                // プレイヤーをジャンプ
                player.jump();
            };
            
            // 攻撃ボタン用(右下配置)
            var bBtn = new Button(SCREEN_WIDTH - 70, SCREEN_HEIGHT - 55, 'b');
    		scene.addChild(bBtn);
            bBtn.ontouchstart = function() {
                // プレイヤーの攻撃
            	var playerAttack = player.attack();
            	playerAttack.scene = scene;
        		scene.addChild(playerAttack);
            }
            
            // スコア
            scoreLabel = new ScoreLabel(10, 10);
            scoreLabel.score = 0;
            scene.addChild(scoreLabel);
            scene.addChild(stage);
            
            // プレイヤーを生成, 表示
            player = new Player();
            player.x = 50;
            player.y = 32;
            player.vx = 0;//右への加速度
            player.vy = 0;//下への加速度
            player.ax = 0;
            player.ay = 0;
            player.pose = 0;
            player.jumping = true;
            player.jumpflg = 0;
//            scene.addChild(player);
            stage.addChild(player);
            stage.addEventListener('enterframe', function(e) {
                this.x = 50 - (player.x + player.ax);
                g_idx = this.x;
            });
            
        };
        
        // シーン更新時の処理
        scene.onenterframe = function() {
            // アイテムを生成, 表示
            if (game.frame % 50 == 0) {
                var r = Math.floor(Math.random()*100);
                var item = null;
                if (r < 10) {
                    item = new Diamond();
                }
                else {
                    item = new Coin();
                }
                item.moveTo(SCREEN_WIDTH+30, Math.random()*(SCREEN_HEIGHT-ENEMY_HEIGHT));
                scene.addChild(item);
            }
            
            // 敵を生成, 表示
            if (game.frame % 30 == 0) {
                var enemy_y = Math.random()*(SCREEN_HEIGHT-ENEMY_HEIGHT);
                if(enemy_y < SCREEN_HEIGHT - (G_HEIGHT * 3) ){
                    var enemy = new Enemy();
                    enemy.moveTo(SCREEN_WIDTH+30, enemy_y);
                    scene.addChild(enemy);

                    // フレームでのキー管理
                    enemy.key = game.frame;
                    enemies[enemy.key] = enemy;
                }
            }
            
            // 足軽を生成, 表示
            if(g_idx == -200){
                ashigaru = new Ashigaru();
                ashigaru.x = p_idx + 180;
                ashigaru.y = SCREEN_HEIGHT - (G_HEIGHT * 4);
                ashigaru.vx = 0;//右への加速度
                ashigaru.vy = 0;//下への加速度
                ashigaru.ax = 0;
                ashigaru.ay = 0;
                ashigaru.pose = 0;
                stage.addChild(ashigaru);
            }
        };
        return scene;
    };

    game.start();
};

// ボタンオブジェクト
var Button = enchant.Class.create(enchant.Sprite, {
	initialize: function(x, y, mode) {
		enchant.Sprite.call(this, 50, 50);
		
		// ボタンを最前面にするため、スタイル要素をブロック化
        if (enchant.CanvasLayer) {
            this._element = 'div';
        }
        
        if(mode == 'a'){
            this.image = game.assets[BTN_JUMP];
        }else if(mode == 'b'){
            this.image = game.assets[BTN_SHURIKEN];
        }else{
            this.image = game.assets[BTN_IMAGE];
        }
        
		this.x = x;
		this.y = y;
		this.buttonMode = mode;
		
		// ボタン要素の最前面指定
        var style = this._style;
        style["z-index"] = 100;
	}
});

/*
 * 攻撃処理
 */
var PlayerAttack = Class.create(enchant.Sprite, {
    // 初期化処理
    initialize: function(x, y) {
        // 親の初期化呼び出し
        Sprite.call(this, ATTACK_WIDTH, ATTACK_HEIGHT); // 画像サイズは後々変更すべき
        this.image = game.assets[ATTACK_IMAGE];
        this.x = x + 10;
        this.y = y - 5;
        //this.frame = 3;
        this.moveSpeed = 20;
    },
    remove: function() {
    	this.scene.removeChild(this);
    	delete this;
    },
    
    // 更新処理
    onenterframe: function() {
      	this.x = this.x + 3;
      	if (this.x > SCREEN_WIDTH) {
      		this.remove();
      		return;
      	}
      	
      	for (var i in enemies) {
            if (enemies[i].intersect(this)) {
            	var blast = new Blast(this.x + 25, this.y + 10, this.scene);
      			enemies[i].remove(this.scene);
      			this.remove();
      	    }
      	}
    }
});

// 爆発エフェクト
var Blast = enchant.Class.create(enchant.Sprite, {
	initialize: function(x, y, scene) {
		enchant.Sprite.call(this, 16, 16);
		this.x = x;
		this.y = y;
		this.image = game.assets[ATTACK_EFFECT];
		this.time = 0;
		
		this.duration = 20;
		this.frame = 0;
		scene.addChild(this);
    },
    
    remove: function() {
    	this.scene.removeChild(this);
    	delete this;
    },
    
    // 更新処理
    onenterframe: function() {
      	this.time++;
      	
        this.frame = Math.floor(this.time / this.duration * 5);
        if (this.time === this.duration) {
        	this.remove();
        }
    }
});

/*
 * ゲームオーバー処理
 */
var gameOver = function(rst) {
    var score = scoreLabel.score;
    var msg = score + "point 獲得!" + rst;
    game.end(score, msg);
};

/*
 * ゲームクリア処理
 */
var GameClear = Class.create(Sprite, {
	initialize: function(x, y, scene) {
		enchant.Sprite.call(this, SCREEN_WIDTH - 55, 47);
		this.image = game.assets[CLEAR_IMAGE];
		this.x = x;
		this.y = y;
		// 半透明にする
		this.opacity = 1;
		scene.addChild(this);
		game.stop();
    }
});

/*
 * プレイヤー
 */
var Player = Class.create(Sprite, {
    // 初期化処理
    initialize: function() {
        Sprite.call(this, PLAYER_WIDTH, PLAYER_HEIGHT);
        this.image = game.assets[PLAYER_IMAGE1];
        this.frame = 0;
        this.vy = 0;    // 移動速度
        this.time = randfloat(0, 360) | 0;
        this.positionX = 50; // 画像のＸ座標固定位置
    },
    // 更新処理
    onenterframe: function() {

        var Rectangle = enchant.Class.create({
            initialize: function(x, y, width, height) {
                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;
            },
            right: {
                get: function() {
                    return this.x + this.width;
                }
            },
            bottom: {
                get: function() {
                    return this.y + this.height;
                }
            }
        });
        
        //プレイヤーの座標とサイズ
        var dest = new Rectangle(
            this.x, this.y,
            this.width, this.height
        );
        
        //プレイヤーの左右の下と地面が衝突した場合
        if(map.hitTest(dest.x,dest.bottom) || map.hitTest(dest.right,dest.bottom)){
            dest.y = dest.y;
            if(this.ay == 0){
//document.getElementById('debug').innerHTML = "☆x:" + this.x + "<br>y:" + this.y + "<br>jumpflg:" + this.jumpflg;
                this.jumpflg = 0;
            }
        }else{
            dest.y += PLAYER_GRAVIRY;
        }

        //プレイヤーの左上と天井が衝突した場合
        if(map.hitTest(dest.x,dest.y)){
            dest.y -= PLAYER_JUMP;
            this.ay += PLAYER_GRAVIRY;
        }else{
            dest.y = dest.y;
        }
        
        //プレイヤーが右上と壁が衝突した場合
        if(map.hitTest(dest.right,dest.y)){
            dest.x -= 0;
            this.ax += 2;
        }else{
            dest.x += 2;
            this.ax = 0;
        }
        
        //ジャンプしてない場合
        if (!this.jumping) {
            if(this.ay < 0){
                this.ay++;
            }
            if(this.ay == 0){
               this.ay = this.ay;
            }
        }

        this.jumping = false;

        this.x = dest.x;
        p_idx = this.x;
        this.y = dest.y + this.ay;
        
//document.getElementById('debug').innerHTML = "x:" + this.x + "<br>y:" + this.y + "<br>jumpflg:" + this.jumpflg;
        
//document.getElementById('debug').innerHTML = "hit:" + map.hitTest(dest.right, dest.bottom - 2);
//document.getElementById('debug').innerHTML = "right:" + dest.right + " btm:" + (dest.bottom-2) + " width:" + dest.width + " height:" + dest.height;
//document.getElementById('debug').innerHTML += " mapx:" + map.x + " mapy:" + map.y;
        
        //穴に落ちた時
        if (this.y > 320) {
            //game.assets['gameover.wav'].play();
            var score = Math.round(this.x);
            this.frame = 3;
            this.vy = -20;
            this.addEventListener('enterframe', function() {
                this.vy += 2;
                this.y += Math.min(Math.max(this.vy, -10), 10);
                // playerの落下判定を修正
                if (this.y > 310) {
                    game.end(score, score + 'mで死にました');
                }
                this.removeEventListener('enterframe', arguments.callee);
            });
        }
        // 画面からはみ出た際はゲームオーバー
        if ( (this.x+this.width) < 0)   {
            gameOver("画面外に出てしまいました. 残念!!");
        }
        
        // フレームアニメーション
        if (this.time % 5 == 0) {
            if(this.image == game.assets[PLAYER_IMAGE1]){
                this.image = game.assets[PLAYER_IMAGE2];
            }else if(this.image == game.assets[PLAYER_IMAGE2]){
                this.image = game.assets[PLAYER_IMAGE3];
            }else if(this.image == game.assets[PLAYER_IMAGE3]){
                this.image = game.assets[PLAYER_IMAGE4];
            }else{
                this.image = game.assets[PLAYER_IMAGE1];
            }
        }
        // タイムを進める
        ++this.time;
        
    },
    // ジャンプ処理
    jump: function() {
        // 移動値を設定
        if(this.jumpflg == 0){
            this.jumping = true;
            this.jumpflg = 1;
            this.ay = PLAYER_JUMP;
        }
    },
    
    // 攻撃処理
    attack: function() {
        // 移動値を設定
    	var s = new PlayerAttack(this.positionX, this.y);
        return s;
    },
});


/*
 * 敵
 */
var Enemy = Class.create(Sprite, {
    // 初期化処理
    initialize: function() {
        // 親の初期化呼び出し
        Sprite.call(this, 39, 25);
        this.image = game.assets[HINOTAMA_IMAGE1];
        this.time = randfloat(0, 360) | 0;
    },
    // 更新処理
    onenterframe: function() {
        // 移動
        this.x += ENEMY_SPEED;
        this.y += Math.cos(this.time*5*Math.PI/180);
        
        // フレームアニメーション
        //if (this.time % 5 == 0) {
        //    this.frame += 1;
        //    this.frame %= 3;
        //}

        // フレームアニメーション
        if (this.time % 5 == 0) {
            if(this.image == game.assets[HINOTAMA_IMAGE1]){
                this.image = game.assets[HINOTAMA_IMAGE2];
            }else{
                this.image = game.assets[HINOTAMA_IMAGE1];
            }
        }
        
        // プレイヤーとの衝突判定
        if (this.within(player, ENEMY_HIT_LENGTH)) {
            gameOver("火の玉と衝突してしまいました. 残念!!");
        }
        
        // 削除処理
        if (this.x < -40) {
            this.parentNode.removeChild(this);
        }
        
        // タイムを進める
        ++this.time;
    },
    
    // 敵キャラ削除処理(rootSceneから各Scene管理に変更)
    remove: function(scene) {
    	scene.removeChild(this);
    	delete enemies[this.key];
    	delete this;
    }
});

var BossTemplete = Class.create(Enemy, {
    // 初期化処理
    initialize: function(img) {
        // 親の初期化呼び出し
        Sprite.call(this, 80, 80);
        this.image = img;
        this.x = 200;
        this.y = BACKGROUND_HEIGHT - 135;
        this.hitPoint = 50;
    },
});

/*
 * 大蛇
 */
var Orochi = Class.create(BossTemplete, {
    // 初期化処理
    initialize: function() {
    	BossTemplete.call(this, game.assets[OROCHI_IMAGE]);
		this.frame = 11;
        this.framePoint = 0;
        // プレイヤーへの攻撃フラグ
        this.attackFlg = false;
        // 地中へ潜るフラグ
        this.retractFlg = false;
        // ダメージを受けるフラグ
        this.damageFlg = false;
        // 地上へ出現フラグ
        this.comeOutFlg = true;
        // 地上への出現カウント
        this.comeOutCount = 30;
        
        // ゲームクリア用フラグ
        this.clearFlg = 0;
    },
    // 更新処理
    onenterframe: function() {
       	// モンスター出現ブロック
    	if (this.comeOutFlg) {
    		// モンスターの出現時間調整
    		if (this.comeOutCount <= 0) {
    	        // フレームアニメーション
        	    if (game.frame % 6 == 0) {
        	    	if (this.frame == 11) {
        	    		this.x = 200;
        	    		this.frame = 0;
        	    	} else if (this.frame <= 1 || (this.frame >= 4 && this.frame < 6)) {
        			    this.frame++;
        		    } else if(this.frame == 2) {
        			    this.frame *= 2;
        			    this.damageFlg = true; // 地中から出てきたフレームの時のみダメージを受ける。
            		} else {
    	        		switch (this.frame) {
        	        		case 6:
    	        	    	case 10:
    	        			this.framePoint = this.frame;
    	    	    		this.frame = 9;
    	    		    	break;
        	    		case 9:
    	        			if (this.framePoint == 6) {
    	            			this.frame = 10;
    	    	    		} else if (this.framePoint % 5 === 0) {
    	    		    		this.frame = 6;
    	    			    }
        	    			break;
    	        		case 7:
        	       		case 8:
    	    	    		this.frame--;
    	    		    }
            		}
            	}
        	
            	// モンスターの攻撃タイミング
            	if (game.frame % 300 === 0) {
    	    		this.frame = 10;
            		this.comeOutFlg = false;
            		this.damageFlg = false;
        		    this.retractFlg = true;
            	}    			
    		} else {
    			this.comeOutCount--;
    		}
        // モンスターの攻撃アニメーションブロック
		} else if (this.attackFlg) {
            // モンスターの攻撃移動アニメーションブロック
           	if (game.frame % 3 == 0 && player.positionX + 15 <= this.x) {
    	    	switch (this.frame){
    	    	case 1:
    	    		this.frame = 4;
    	    		break;
    	    	case 4:
    	    		this.frame = 0;
    	    		break;
    	    	case 6:
    	    		this.frame = 1;
    	    		break;
    	    	default:
   		    		if (this.frame <= 2) {
   		        		this.frame = 3;
   		    		} else {
   		    			this.frame = 2;
   		    		}
   	    	    	this.x -= 15;
        		}
            // プレイヤーへの攻撃アニメーションブロック
            } else if (game.frame % 6 == 0 && player.positionX + 15 > this.x) {
        		switch (this.frame){
    	    	case 0:
    	    		this.frame = 1;
    	    		break;
    	    	case 1:
    	    		this.frame = 5;
    	    		break;
    	    	case 2:
    	    	case 3:
    	    		this.frame = 0;
    	    		break;
    	    	case 5:
    	    		this.frame = 6;
    	    		break;
    	    	case 6:
    	    		this.frame = 9;
    	    		break;
    	    	case 9:
    	    		this.frame = 10;
    	    		break;
    	    	case 10:
    	    		this.attackFlg = false;
    	    		this.retractFlg = true;
    	    		break;
        		}
            }
        	
        // モンスターの潜りアニメーションブロック
    	} else if(this.retractFlg) {
	        // フレームアニメーション
    		if (game.frame % 3 == 0) {
        		switch (this.frame){
    	    	case 0:
       	    		if (player.positionX + 10 > this.x) {
        	    		this.x = -200;
        	    		this.comeOutCount = 50;
        	    		this.comeOutFlg = true;
        	            this.frame = 11;
    	    		} else {
    	    			this.attackFlg = true;	
    	    		}
    	    		this.retractFlg = false;
    	    		break;
    	    	case 1:
    	    		this.frame = 0;
    	    		break;
    	    	case 6:
    	    		this.frame = 1;
    	    		break;
    	    	case 10:
    	    		this.frame = 6;
    	    		break;
        		}
            }
    	}
    	
        // プレイヤーとの衝突判定
        if (this.frame == 10 && this.within(player, ENEMY_HIT_LENGTH - 60)) {
            gameOver("大蛇の攻撃を受けてしまいました。 残念!!");
        }
    },
    
    // ボスキャラ削除処理
    remove: function(scene) {
    	if (this.damageFlg && this.clearFlg === 0) {
	    	if (this.hitPoint <= 0) {
		    	scene.removeChild(this);
		    	delete this;
		        scoreLabel.score += 3000;
		        this.clearFlg = 1;
	    	} else {
	    		this.frame = 8;
	    		this.hitPoint--;
	    	}
    	}
    	
    	if (this.clearFlg === 1) {
    		this.clearFlg = 2;
    		new GameClear(25, 150, scene);
    	}
    }
});

/*
 * アイテム
 */
var Item = Class.create(Sprite, {
    // 初期化処理
    initialize: function() {
        Sprite.call(this, ITEM_WIDTH, ITEM_HEIGHT);
        this.image = game.assets[ICON_IMAGE];
    },
    // 更新処理
    onenterframe: function() {
        // 移動
        this.x += ITEM_SPEED;
        
        // 衝突判定
        if (this.intersect(player)) {
            // ヒットイベントを発行する
            var e = new enchant.Event("hit");
            this.dispatchEvent(e);
        }
        
        // 削除処理
        if (this.x < -40) {
            this.parentNode.removeChild(this);
        }
    },
    // ヒット時処理
    onhit: function(e) {
        console.log("hit!");
    },
});


/*
 * コイン
 */
var Coin = Class.create(Item, {
    // 初期化処理
    initialize: function() {
        Item.call(this);
        this.frame = COIN_FRAME;
    },
    // 更新処理
    onhit: function(e) {
        // スコアアップラベルを生成, 表示
        var label = new ScoreUpLabel(COIN_POINT);
        label.moveTo(this.x, this.y);
        game.rootScene.addChild(label);
        
        // 削除
        this.parentNode.removeChild(this);
        // スコア加算
        scoreLabel.score += COIN_POINT;
    },
});


/*
 * ダイヤモンド
 */
var Diamond = Class.create(Item, {
    // 初期化処理
    initialize: function() {
        Item.call(this);
        this.frame = DIAMOND_FRAME;
    },
    // ヒット時処理
    onhit: function(e) {
        // スコアアップラベルを生成, 表示
        var label = new ScoreUpLabel(DIAMOND_POINT);
        label.moveTo(this.x, this.y);
        game.rootScene.addChild(label);
        
        // 削除
        this.parentNode.removeChild(this);
        // スコア加算
        scoreLabel.score += DIAMOND_POINT;
    },
});


/*
 * スコアアップ
 */
var ScoreUpLabel = Class.create(MutableText, {
    // 初期化処理
    initialize: function(score) {
        MutableText.call(this);
        
        this.text   = '+' + score;
        this.time   = 0;
    },
    // ヒット時処理
    onenterframe: function() {
        // 移動
        this.y -= 0.1;
        // 透明度
        this.opacity = 1.0 - (this.time/30);
        
        // 削除
        if (this.time > 30) {
            this.parentNode.removeChild(this);
        }
        
        this.time += 1;
    },
});


/*
 * 地面
 */
var map = null;


/*
 * 足軽
 */
var Ashigaru = Class.create(Sprite, {
    // 初期化処理
    initialize: function() {
        Sprite.call(this, ASHIGARU_WIDTH, ASHIGARU_HEIGHT);
        this.image = game.assets[ASHIGARU_IMAGE1];
        this.frame = 0;
//        this.vy = 0;    // 移動速度
        this.time = randfloat(0, 360) | 0;
//        this.positionX = 50; // 画像のＸ座標固定位置
    },
    // 更新処理
    onenterframe: function() {

        // プレイヤーとの衝突判定
        if (this.within(player, ENEMY_HIT_LENGTH)) {
            gameOver("足軽と衝突してしまいました. 残念!!");
        }
        
        var Rectangle = enchant.Class.create({
            initialize: function(x, y, width, height) {
                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;
            },
            right: {
                get: function() {
                    return this.x + this.width;
                }
            },
            bottom: {
                get: function() {
                    return this.y + this.height;
                }
            }
        });
        
        //プレイヤーの座標とサイズ
        var dest = new Rectangle(
            this.x, this.y,
            this.width, this.height
        );
        
        //プレイヤーの左右の下と地面が衝突した場合
        if(map.hitTest(dest.x,dest.bottom) || map.hitTest(dest.right,dest.bottom)){
            dest.y = dest.y;
        }else{
            dest.y += PLAYER_GRAVIRY;
        }

        //プレイヤーの左上と天井が衝突した場合
        if(map.hitTest(dest.x,dest.y)){
            dest.y -= PLAYER_JUMP;
            this.ay += PLAYER_GRAVIRY;
        }else{
            dest.y = dest.y;
        }
        
        //プレイヤーが右上と壁が衝突した場合
        if(map.hitTest(dest.right,dest.y)){
            dest.x -= 0;
            this.ax += 2;
        }else{
            dest.x -= 2;
            this.ax = 0;
        }
        
        if(this.ay < 0){
            this.ay++;
        }
        if(this.ay == 0){
           this.ay = this.ay;
        }

        this.x = dest.x;
        this.y = dest.y + this.ay;
        
//document.getElementById('debug').innerHTML = "x:" + this.x + "<br>y:" + this.y + "<br>jumpflg:" + this.jumpflg;
        
//document.getElementById('debug').innerHTML = "hit:" + map.hitTest(dest.right, dest.bottom - 2);
//document.getElementById('debug').innerHTML = "right:" + dest.right + " btm:" + (dest.bottom-2) + " width:" + dest.width + " height:" + dest.height;
//document.getElementById('debug').innerHTML += " mapx:" + map.x + " mapy:" + map.y;
        
        //穴に落ちた時
        if (this.y > 320) {
            //game.assets['gameover.wav'].play();
            var score = Math.round(this.x);
            this.frame = 3;
            this.vy = -20;
            this.addEventListener('enterframe', function() {
                this.vy += 2;
                this.y += Math.min(Math.max(this.vy, -10), 10);
                if (this.y > 320) {
                    //game.end(score, score + 'mで死にました');
                }
            });
            this.removeEventListener('enterframe', arguments.callee);
        }
        // 画面からはみ出た際はゲームオーバー プレイヤーの座標が常に動くため０で判定できない。
        if ( (this.x+this.width) < 0)   {
            //gameOver("画面外に出てしまいました. 残念!!");
        }
        
        // フレームアニメーション
        if (this.time % 5 == 0) {
            if(this.image == game.assets[ASHIGARU_IMAGE1]){
                this.image = game.assets[ASHIGARU_IMAGE2];
            }else if(this.image == game.assets[ASHIGARU_IMAGE2]){
                this.image = game.assets[ASHIGARU_IMAGE3];
            }else if(this.image == game.assets[ASHIGARU_IMAGE3]){
                this.image = game.assets[ASHIGARU_IMAGE4];
            }else{
                this.image = game.assets[ASHIGARU_IMAGE1];
            }
        }
        // タイムを進める
        ++this.time;
        
    },
});

